from django.core.management import BaseCommand

from teachers.models import Teacher


class Command(BaseCommand):
    help = 'just write command with some number'

    def handle(self, *args, **options):
        Teacher.top_ten()

