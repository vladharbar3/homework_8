from django.core.management.base import BaseCommand
from teachers.models import Teacher


class Command(BaseCommand):
    help = 'Write this command with some number'

    def add_arguments(self, parser):
        parser.add_argument('count')

    def handle(self, *args, **options):
        count = int(options['count'])
        Teacher.generate_teachers(count)
