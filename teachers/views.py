from django.http import HttpResponse, HttpResponseRedirect
from django.views.decorators.csrf import csrf_exempt

from teachers.forms import TeacherCreateForm
from teachers.models import Teacher
from teachers.utils import render_list_html


def get_teachers(request):
    qs = Teacher.objects.all()
    params = [
        'first_name',
        'first_name__startswith',
        'first_name__endswith',
        'first_name__contains',
        'last_name',
        'age',
        'age__gt',
        'age__lt',
        'email',
        'birth_day',
        'phone_number',
    ]

    query = {}

    form_main = f"""<form>
           <p>Search teachers</p>
           <p>
               <input type="text" name="first_name" 
                   value="{request.GET.get('first_name', '')}" 
                   placeholder="Input First Name">
           </p>
           <p>
               <input type="text" name="last_name" 
                   value="{request.GET.get('last_name', '')}"
                   placeholder="Input Last Name">
           </p>
           <p>
               <input type="number" name="age" 
                   value="{request.GET.get('age', '')}"
                   placeholder="Input Age">
           </p>

            <p>
               <input type="text" name="email" 
                   value="{request.GET.get('email', '')}"
                   placeholder="Input Email">
           </p>6
           
           <p>
               <input type="date" name="birth_date" 
                   value="{request.GET.get('birth_date', '')}"
                   placeholder="Input birth date">
           </p>
          
           <p>
               <input type="number" name="phone_number" 
                   value="{request.GET.get('phone_number', '')}"
                   placeholder="Input phone number">
           </p>
           
           <p><button type="submit">Search</button></p>
       </form>
       """

    for param_name in params:
        param_value = request.GET.get(param_name)
        if param_value:
            query[param_name] = param_value

    try:
        qs = qs.filter(**query)
    except ValueError as e:
        return HttpResponse(f"Error wrong input data. {str(e)}", status=400)
    return render_list_html(qs[:20], form_main)


@csrf_exempt
def create_teachers(request):
    if request.method == 'POST':
        form_main = TeacherCreateForm(request.POST)

        if form_main.is_valid():
            form_main.save()
            return HttpResponseRedirect('/teachers/')
    else:
        form_main = TeacherCreateForm()

    html = f"""
        <form method="post">
            {form_main.as_p()}
            <p><button type="submit">Create Teachers</button></p>
        </form>
    """

    return HttpResponse(html)
