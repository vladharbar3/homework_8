import random

from django.db import models, connections
from faker import Faker


# Create your models here.


class Teacher(models.Model):
    first_name = models.CharField(max_length=64, null=False)
    last_name = models.CharField(max_length=64, null=False)
    age = models.IntegerField(default=20)
    email = models.CharField(max_length=63, null=False)
    birth_date = models.DateField()
    phone_number = models.CharField(max_length=10, null=False)

    @classmethod
    def generate_teachers(cls, count):
        faker = Faker()

        for _ in range(count):
            s = Teacher()
            s.first_name = faker.first_name()
            s.last_name = faker.last_name()
            s.age = random.randint(18, 80)
            s.email = faker.ascii_company_email()
            s.birth_date = faker.date()
            s.phone_number = faker.country_calling_code()
            s.save()

    def __str__(self):
        return f"Teacher({self.id}) {self.first_name} {self.last_name} " \
               f"{self.age} {self.email} {self.birth_date} {self.phone_number}"

    @classmethod
    def top_ten(cls):
        sql_text = 'SELECT * FROM teachers_teacher ORDER BY first_name LIMIT 10;'

        with connections['default'].cursor() as cursor:
            cursor.execute(sql_text)
            cursor.fetchall()


